# fedora-setup

ansible-playbook fedora-setup.yaml

Additionnal manual tasks:
* Download and install fonts (I use Pragmata Pro family font)
* WSL:
    * Install and configure Windows Terminal with files/windows_terminal.json example
    * Change default wsl user on connect
    * Change default path on connect (terminal settings)